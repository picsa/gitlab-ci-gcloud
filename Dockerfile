FROM docker:dind

RUN apk add --no-cache \
        git \
        curl \
        bash \
        python

# Install Google Cloud SDK
RUN curl https://sdk.cloud.google.com | bash

COPY scripts/ /usr/bin/
